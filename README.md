# Tp6 K8s

## Prerequisites

Before we begin, you need to have the following software installed on your computer:

- Box rocky9 cloud with Python and Ansible
- VirtualBox or another virtualization software
- Vagrant
- Ansible

## Setting up the environment

1. Clone or download the project repository to your local machine.
2. Open a terminal or command prompt window and navigate to the project directory.
3. Go to `vagrant` directory. (`cd vagrant`)
4. Run the command `vagrant up` to launch the virtual machine.

## Verifying Variables

Before launching the project with Ansible, it's important to verify the variables defined in the `roles/k8s/defaults/main.yml` file. This file contains the default values for the various variables used in the Ansible playbook. If changes are needed, you can make them directly in this file.

## Launching the project with Ansible

1. Go to `ansible` directory. (`cd ansible`)
2. Run the command `ansible-playbook -i inventories/production/hosts.ini playbooks/main.yml` to launch the project using Ansible.

## Changing IP addresses

If you need to change the IP addresses in the `Vagrantfile`, you also need to modify the following files in `ansible/roles/k8s/files/`:

- `k8s/config.yaml`
- `metallb/address-pool.yaml`

This is because these files contain references to the IP addresses that are specified in the `Vagrantfile`. Failing to update these files will result in errors when you launch the project with Ansible.

## Conclusion

By following these steps, you should be able to easily launch a project using Vagrant and Ansible. If you have any questions or encounter any issues, don't hesitate to reach out for help.
